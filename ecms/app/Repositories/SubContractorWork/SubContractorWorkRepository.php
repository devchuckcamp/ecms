<?php

namespace App\Repositories\SubContractorWork;

use App\Models\SubContractorWork;
use App\Models\SubContractorWorkPayments;
use App\Models\LubricantMonitoring;
use App\Models\Monitoring;
use Core\Repository;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SubContractorWorkRepository implements Repository\SubContractorWork\SubContractorWorkRepository
{

    public function get_all_subcontractor_works()
    {
        return SubContractorWork::with(['created_by','updated_by','equipment','subcontractorwork_payments'])->toArray();
    }

    public function get_subcontractor_work_by_id($subcontractor_work_id)
    {
        return SubContractorWork::with(['created_by','updated_by','equipment','subcontractor','subcontractorwork_payments'])->find($subcontractor_work_id)->toArray();
    }

    public function get_subcontractor_works(array $filters, $page, $per_page) : array
    {
        $model = new SubContractorWork;
        $subcontractor_work = $model->with(['created_by','updated_by','equipment','subcontractor']);

        if (isset($filters['q']))
        {
            $subcontractor_work->where(function($q) use ($filters) {
                $q->orWhere('transaction_no', 'like', '%'. $filters['q'] .'%');
                $q->orWhere('project', 'like', '%'. $filters['q'] .'%');
            });
        }

        if (isset($filters['subcontractor']))
        {
            $subcontractor_work->where('subcontractor', '=', $filters['subcontractor']);
        }

        if (isset($filters['equipment']))
        {
            $subcontractor_work->where('equipment', '=', $filters['equipment']);
        }

        if (isset($filters['date_from']) && isset($filters['date_to']))
        {
            $dates = [new Carbon($filters['date_from']), new Carbon($filters['date_to'])];
            $subcontractor_work->whereBetween('transaction_date', $dates);
        }

        $subcontractor_work->orderBy('id', 'desc');

        $subcontractor_work = $subcontractor_work->paginate($per_page);

        return $subcontractor_work->toArray();
    }

    public function get_pe_subcontractor_works(array $filters) : array
    {
        $model = new SubContractorWork;
        $subcontractor_work = $model->with(['created_by','updated_by','equipment','subcontractor','subcontractorwork_payments']);

        if (isset($filters['subcontractor']))
        {
            $subcontractor_work->where('subcontractor', '=', $filters['subcontractor']);
        }

        if (isset($filters['date_from']) && isset($filters['date_to']))
        {
            $dates = [new Carbon($filters['date_from']), new Carbon($filters['date_to'])];
            $subcontractor_work->whereBetween('transaction_date', $dates);
        }

        $subcontractor_work->orderBy('id', 'desc');

        $subcontractor_work = $subcontractor_work->get();

        return $subcontractor_work->toArray();
    }

    public function get_subcontractor_works_by_search_id(array $filters, $page, $per_page) : array
    {

        if (isset($filters['subcontractor']))
        {
            $model = new SubContractorWork;
            $subcontractor_work = $model->with(['created_by','updated_by','equipment','subcontractor']);

            $subcontractor_work->where('subcontractor', '=', $filters['subcontractor']);

            if (isset($filters['date_from']) && isset($filters['date_to']))
            {
                $dates = [new Carbon($filters['date_from']), new Carbon($filters['date_to'])];
                $subcontractor_work->whereBetween('transaction_date', $dates);
            }

            $subcontractor_work->orderBy('id', 'desc');
            $subcontractor_work = $subcontractor_work->paginate($per_page);
            $subcontractor_work = $subcontractor_work->toArray();
        }

        else {
            $subcontractor_work = [];
        }

        return $subcontractor_work;
    }

    public function store_subcontractor_work(array $subcontractor_work) : array
    {

        DB::beginTransaction();

        /**
         * Store/Save SubContractorWork
         */
        $model = new SubContractorWork;
        $model->transaction_no = $subcontractor_work['transaction_no'];
        $model->transaction_date = new Carbon($subcontractor_work['transaction_date']);
        $model->created_user_id = $subcontractor_work['created_user_id'];
        $model->updated_user_id = $subcontractor_work['updated_user_id'];

        $model->subcontractor = $subcontractor_work['subcontractor'];
        $model->equipment = $subcontractor_work['equipment'];
        $model->project = $subcontractor_work['project'];
        $model->scope_of_work = $subcontractor_work['scope_of_work'];
        $model->project_start_date = new Carbon($subcontractor_work['project_start_date']);
        $model->contract_amount = $subcontractor_work['contract_amount'];
        $model->total_amount_due_left = $subcontractor_work['contract_amount'];

        $model->save();

        DB::commit();

        return $model->toArray();
    }

    public function update_subcontractor_work($subcontractor_work_id, Request $data) : array
    {

        DB::beginTransaction();

        if($data->warranty) {
            /**
             * Edit/Update SubContractorWork: warranty field only
             */
            SubContractorWork::where('id', $subcontractor_work_id)->update([

                'transaction_date' => new Carbon($data->transaction_date),
                'updated_user_id' => \Auth::user()->id,

                'subcontractor' => $data->subcontractor,
                'equipment' => $data->equipment,
                'warranty' => $data->warranty,

                'project' => $data->project,
                'project_start_date' => new Carbon($data->project_start_date),
                'scope_of_work' => $data->scope_of_work
            ]);

        } else {

            $swp_list = (new SubContractorWorkPayments)->lockForUpdate()->where('subcontractorwork_id', $subcontractor_work_id)->get();
            $current_work = (new SubContractorWork)->lockForUpdate()->where('id', $subcontractor_work_id)->first();

            $current_total_previous_paid_amount = 0;
            $current_total_current_paid_amount = 0;
            $current_total_amount_due_left = 0;

            /**
             * Simple Calculation
             */
            if(!$swp_list->isEmpty()){

                foreach ($swp_list as $key => $swp){
                    $current_total_previous_paid_amount += $swp['previous_paid_amount'];
                    $current_total_current_paid_amount += $swp['current_paid_amount'];
                    $current_total_amount_due_left += $swp['amount_due_left'];
                }

                if($current_total_previous_paid_amount == 0)
                    $current_total_previous_paid_amount += $data->amount_to_pay;

                $current_total_current_paid_amount += $data->amount_to_pay;
                $current_total_amount_due_left = bcsub($current_work->contract_amount, $current_total_current_paid_amount);

                $previous_paid = $swp_list->last()->current_paid_amount;
                $amount_due_left = bcsub($current_work->contract_amount, $current_total_current_paid_amount);

            } else {
                $previous_paid = 0;
                $amount_due_left = bcsub($current_work->contract_amount, $data->amount_to_pay);

                $current_total_current_paid_amount = $data->amount_to_pay;
                $current_total_amount_due_left = $amount_due_left;
            }
            /**
             * Store listing payment
             */
            $swp_model = new SubContractorWorkPayments;

            $swp_model->subcontractorwork_id = $current_work->id;

            $swp_model->current_paid_amount = $data->amount_to_pay;
            $swp_model->contract_amount = $current_work->contract_amount;
            $swp_model->created_user_id = \Auth::user()->id;

            $swp_model->previous_paid_amount = $previous_paid;
            $swp_model->amount_due_left = $amount_due_left;

            $swp_model->save();


            /**
             * Edit/Update SubContractorWork
             */
            SubContractorWork::where('id', $subcontractor_work_id)->update([

                'transaction_date' => new Carbon($data->transaction_date),
                'updated_user_id' => \Auth::user()->id,

                'subcontractor' => $data->subcontractor,
                'equipment' => $data->equipment,

                'total_previous_paid_amount' => $current_total_previous_paid_amount,
                'total_current_paid_amount' => $current_total_current_paid_amount,
                'total_amount_due_left' => $current_total_amount_due_left,

                'project' => $data->project,
                'project_start_date' => new Carbon($data->project_start_date),
                'scope_of_work' => $data->scope_of_work
            ]);
        }

        DB::commit();

        return SubContractorWork::find($subcontractor_work_id)->toArray();
    }

    /**
     * Check subcontractor_work stock and use
     */

    public function stocked_subcontractor_work()
    {
        return Monitoring::where('code',Monitoring::FUEL_STOCK_CODE)->first();
    }

    public function used_subcontractor_work()
    {
        return Monitoring::where('code',Monitoring::FUEL_USE_CODE)->first();
    }

    public function available_subcontractor_work($out, $update = true)
    {

        if(isset($out) && $out && $update)
            return bcadd(bcsub(($this->stocked_subcontractor_work())->value, ($this->used_subcontractor_work())->value), $out);

        return bcsub(($this->stocked_subcontractor_work())->value, ($this->used_subcontractor_work())->value);
    }

    public function updated_stocked_subcontractor_work($in, $current_in)
    {
        return bcadd(bcsub(($this->stocked_subcontractor_work())->value, $in), $current_in);
    }

    /**
     * @important
     * Update monitoring table subcontractor_work stock and use
     */
    public function update_monitoring($value, $type)
    {
        (new Monitoring)->where('code', $type)->update(['value' => $value]);

    }

    /**
     * @charts
     * Queries subcontractor_work table for charts
     * @subcontractor_work
     */
    public function subcontractor_work_chart_monthly_usage($filters)
    {
        $subcontractor_work = new SubContractorWork;

        if(isset($filters) && $filters){
            /**
             * Wtih filters
             */
            $end = (new Carbon())->parse($filters['date_to'])->addMonth()->toDateTimeString();
            $start = (new Carbon())->parse($filters['date_from'])->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        } else {
            /**
             * Without filters
             */
            $today = new Carbon();
            $end = $today->addMonth()->toDateTimeString();
            $start = $today->subMonth(3)->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        }

        $subcontractor_works =  $subcontractor_work
                ->selectRaw("CONCAT(year(`transaction_date`), '-', month(`transaction_date`)) as monthYear, month(`transaction_date`) as month, sum(IFNULL(`out`,0)) as total_consume")
                ->whereRaw( "month(`transaction_date`) in (". $months_in .")")
                ->groupBy("monthYear")
                ->groupBy("month")
                ->get()->toArray();

        $latest_data = [];

        foreach ($dates as $date){
            $latest_data[$date['year-month']] = [
                'year' => $date['year'],
                'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                'total_consume' => 0,
            ];
            foreach ($subcontractor_works as $subcontractor_work){
                if($subcontractor_work['monthYear'] == $date['year-month']){
                    $latest_data[$date['year-month']] = [
                        'year' => $date['year'],
                        'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                        'total_consume' => $subcontractor_work['total_consume'],
                    ];
                }
            }
        }

        $data = ['labels'=>[], 'datasets'=>[]];

        foreach (array_unique($latest_data, SORT_REGULAR) as $key => $item) {
            array_push($data['labels'], $item['year'].'-'.$item['month'] );
            array_push($data['datasets'], $item['total_consume'] );
        }

        return $data;

    }

    /**
     * @charts
     * Queries subcontractor_work table for charts
     * @subcontractor_work
     */
    public function subcontractor_work_chart_monthly_stock($filters)
    {
        $subcontractor_work = new SubContractorWork;

        if(isset($filters) && $filters){
            /**
             * Wtih filters
             */
            $end = (new Carbon())->parse($filters['date_to'])->addMonth()->toDateTimeString();
            $start = (new Carbon())->parse($filters['date_from'])->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        } else {
            /**
             * Without filters
             */
            $today = new Carbon();
            $end = $today->addMonth()->toDateTimeString();
            $start = $today->subMonth(3)->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        }

        $subcontractor_works =  $subcontractor_work
            ->selectRaw("CONCAT(year(`transaction_date`), '-', month(`transaction_date`)) as monthYear, month(`transaction_date`) as month, sum(IFNULL(`in`,0)) as total_stock")
            ->whereRaw( "month(`transaction_date`) in (". $months_in .")")
            ->groupBy("monthYear")
            ->groupBy("month")
            ->get()->toArray();

        $latest_data = [];

        foreach ($dates as $date){
            $latest_data[$date['year-month']] = [
                'year' => $date['year'],
                'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                'total_stock' => 0,
            ];
            foreach ($subcontractor_works as $subcontractor_work){
                if($subcontractor_work['monthYear'] == $date['year-month']){
                    $latest_data[$date['year-month']] = [
                        'year' => $date['year'],
                        'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                        'total_stock' => $subcontractor_work['total_stock'],
                    ];
                }
            }
        }

        $data = ['labels'=>[], 'datasets'=>[]];

        foreach (array_unique($latest_data, SORT_REGULAR) as $key => $item) {
            array_push($data['labels'], $item['year'].'-'.$item['month'] );
            array_push($data['datasets'], $item['total_stock'] );
        }

        return $data;

    }

    /**
     * @charts
     * Queries subcontractor_work table for charts
     * @lubricant
     */
    public function lubricant_chart_monthly_usage($filters, $code, $id)
    {
        $lubricant = new LubricantMonitoring;

        if(isset($filters) && $filters){
            /**
             * Wtih filters
             */
            $end = (new Carbon())->parse($filters['date_to'])->addMonth()->toDateTimeString();
            $start = (new Carbon())->parse($filters['date_from'])->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        } else {
            /**
             * Without filters
             */
            $today = new Carbon();
            $end = $today->addMonth()->toDateTimeString();
            $start = $today->subMonth(3)->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        }

        $lubricants =  $lubricant
            ->selectRaw("CONCAT(year(`transaction_date`), '-', month(`transaction_date`)) as monthYear, month(`transaction_date`) as month, sum(IFNULL(`out`,0)) as total_consume")
            ->where('type_of_oil_id', $id)
            ->whereRaw( "month(`transaction_date`) in (". $months_in .")")
            ->groupBy("monthYear")
            ->groupBy("month")
            ->get()->toArray();

        $latest_data = [];

        foreach ($dates as $date){
            $latest_data[$date['year-month']] = [
                'year' => $date['year'],
                'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                'total_consume' => 0,
            ];
            foreach ($lubricants as $lubricant){
                if($lubricant['monthYear'] == $date['year-month']){
                    $latest_data[$date['year-month']] = [
                        'year' => $date['year'],
                        'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                        'total_consume' => $lubricant['total_consume'],
                    ];
                }
            }
        }

        $data = ['labels'=>[], 'datasets'=>[]];

        foreach (array_unique($latest_data, SORT_REGULAR) as $key => $item) {
            array_push($data['labels'], $item['year'].'-'.$item['month'] );
            array_push($data['datasets'], $item['total_consume'] );
        }

        return $data;

    }

    /**
     * @charts
     * Queries subcontractor_work table for charts
     * @lubricant
     */
    public function lubricant_chart_monthly_stock($filters, $code, $id)
    {
        $lubricant = new LubricantMonitoring;

        if(isset($filters) && $filters){
            /**
             * Wtih filters
             */
            $end = (new Carbon())->parse($filters['date_to'])->addMonth()->toDateTimeString();
            $start = (new Carbon())->parse($filters['date_from'])->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        } else {
            /**
             * Without filters
             */
            $today = new Carbon();
            $end = $today->addMonth()->toDateTimeString();
            $start = $today->subMonth(3)->toDateTimeString();
            $dates = \App\Helpers\DateHelper::get_between_dates($start, $end);
            $months = [];

            foreach ($dates as $date){
                $months[] = $date['month'];
            }

            $months_in = implode(",",$months);
        }

        $lubricants =  $lubricant
            ->selectRaw("CONCAT(year(`transaction_date`), '-', month(`transaction_date`)) as monthYear, month(`transaction_date`) as month, sum(IFNULL(`in`,0)) as total_stock")
            ->where('type_of_oil_id', $id)
            ->whereRaw( "month(`transaction_date`) in (". $months_in .")")
            ->groupBy("monthYear")
            ->groupBy("month")
            ->get()->toArray();

        $latest_data = [];

        foreach ($dates as $date){
            $latest_data[$date['year-month']] = [
                'year' => $date['year'],
                'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                'total_stock' => 0,
            ];
            foreach ($lubricants as $lubricant){
                if($lubricant['monthYear'] == $date['year-month']){
                    $latest_data[$date['year-month']] = [
                        'year' => $date['year'],
                        'month' => \App\Helpers\DateHelper::monthsText()[$date['month']],
                        'total_stock' => $lubricant['total_stock'],
                    ];
                }
            }
        }

        $data = ['labels'=>[], 'datasets'=>[]];

        foreach (array_unique($latest_data, SORT_REGULAR) as $key => $item) {
            array_push($data['labels'], $item['year'].'-'.$item['month'] );
            array_push($data['datasets'], $item['total_stock'] );
        }

        return $data;

    }
}
