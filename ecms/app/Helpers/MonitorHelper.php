<?php

namespace App\Helpers;

use App\Models\TypeOfOil;
use App\Models\Monitoring;


class MonitorHelper
{
    /**
     * @return Fuel stock and use
     */
    public function get_total_fuel_stock()
    {
        return ((new Monitoring)->where('code',Monitoring::FUEL_STOCK_CODE)->first())->value;
    }

    public function get_total_fuel_use()
    {
        return ((new Monitoring)->where('code',Monitoring::FUEL_USE_CODE)->first())->value;
    }

    /**
     * @return Lubricant stock and use
     */
    public function get_total_engine_stock()
    {
        return ((new Monitoring)->where('code',Monitoring::ENGINE_STOCK_CODE)->first())->value;
    }

    public function get_total_engine_use()
    {
        return ((new Monitoring)->where('code',Monitoring::ENGINE_USE_CODE)->first())->value;
    }

    public function get_total_hydraulic_stock()
    {
        return ((new Monitoring)->where('code',Monitoring::HYDRAULIC_STOCK_CODE)->first())->value;
    }

    public function get_total_hydraulic_use()
    {
        return ((new Monitoring)->where('code',Monitoring::HYDRAULIC_USE_CODE)->first())->value;
    }

    public function get_total_gear_stock()
    {
        return ((new Monitoring)->where('code',Monitoring::GEAR_STOCK_CODE)->first())->value;
    }

    public function get_total_gear_use()
    {
        return ((new Monitoring)->where('code',Monitoring::GEAR_USE_CODE)->first())->value;
    }

    public function get_total_telluse_stock()
    {
        return ((new Monitoring)->where('code',Monitoring::TELLUSE_STOCK_CODE)->first())->value;
    }

    public function get_total_telluse_use()
    {
        return ((new Monitoring)->where('code',Monitoring::TELLUSE_USE_CODE)->first())->value;
    }



}