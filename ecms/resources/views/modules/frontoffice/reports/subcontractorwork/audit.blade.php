@extends('templates.dmonitoring.master')
@section('title', 'Sub Contractor Monitoring')

@php
    // refactor magic numbers
    $permission = (new \Permission);
    $dateHelper = new \App\Helpers\DateHelper;
    $numFormat = new \App\Helpers\NumberFormatHelper;
    $inputHelper = new \App\Helpers\InputHelper;
    $equipment_list = \App\Helpers\InputHelper::equipment_list();
    $subcontractor_list = \App\Helpers\InputHelper::subcontractor_list();
@endphp

@section('main')
    <div class="main-container-wrapper">
        @include('templates.dmonitoring.includes.page-title', ['page_title' => 'Sub Contractor Work', 'url_back' => 'subcontractor/work'])
        <div class="main-container">

            @include('templates.dmonitoring.includes.subcontractor-submenu')

            <div class="row no-margin margin-bottom-20">
                <div class="col-md-12 filter-wrapper">
                    <div class="col-md-7">
                        {!! Form::open(['method'=>'get', 'class' => 'form-horizontal', 'name' => 'subcontractorwork-filter-form']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::bsDateRangePicker(
                                   ['name' => 'date_from', 'value' => request('date_from'), 'label' => 'From Date', 'attributes' => ['id'=>'date_from'] ],
                                   ['name' => 'date_to', 'value' => request('date_to'), 'label' => 'To Date','attributes' => ['id'=>'date_to'] ]
                               ) }}
                                {{ Form::bsSelect('subcontractor', request('subcontractor'), 'Sub Contractor', $subcontractor_list, ['placeholder' => 'Select Sub Contractor']) }}
                            </div>
                            <div class="col-md-6 no-padding">
                                <a href="{{ url('subcontractor/work/audit') }}" style="margin-top:95px;" class="btn btn-warning margin-bottom-10">Reset</a>
                                {{ Form::submit('Search and Audit', ['class' => 'btn btn-primary btn-filter', 'style'=>'margin-top:85px;']) }}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a href="#" data-url="{{ url('manage/pe/work-audit/print') }}" data-title="Print Date Selection" onclick="CreateModal(this, '#print-modal', 'appendViewModal'); return false;" class="btn btn-warning">
                                    <!-- <i class="fa fa-file-excel-o"></i> EXPORT | --> <i class="fa fa-print"></i> PRINT</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="table-responsive margin-bottom-20">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>PROJECT:</th>
                        <th colspan="2"></th>
                        <th colspan="6" rowspan="3" class="vertical-align-middle">
                            <div class="wraptable-image">
                                <img src="{{ asset('img/dakay-logo.png') }}" class="pull-left" />
                                <span class="align-text-in-image"><strong>Sub Contractor Accomplishment Report</strong></span>
                            </div>
                        </th>
                        <th class="text-center" colspan="2">PERIOD COVERING</th>
                    </tr>
                    <tr>
                        <th>SUBCON:</th>
                        <th colspan="2"></th>
                        <th rowspan="2" class="vertical-align-top">FROM</th>
                        <th rowspan="2" class="vertical-align-top">TO</th>
                    </tr>
                    <tr class="bottom-header">
                        <th>CONTACT NO:</th>
                        <th colspan="2"></th>
                    </tr>
                    <tr>
                        <th>Transaction ID</th>
                        <th>Equipment</th>
                        <th>Scope of Work</th>
                        <th>Contract Amount</th>
                        <th>Accomplishment</th>
                        <th>Previous Paid Amount</th>
                        <th>Total Paid Amount</th>
                        <th>Amount Due</th>
                        <th>Warranty Expiry Date</th>
                        <th>Amount Due Left</th>
                        <th>Remarks</th>
                    </tr>

                    </thead>
                    <tbody>
                    @if(count($subcontractorworks) < 1)
                        <tr>
                            <td colspan="26" class="table-no-records">- No records -</td>
                        </tr>
                    @else
                        @foreach ($subcontractorworks->items() as $subcontractorwork)
                            <tr>
                                <td>{{ $subcontractorwork['transaction_no'] }}</td>
                                <td>{{ $subcontractorwork['equipment'] }}</td>
                                <td>{{ $subcontractorwork['scope_of_work'] }}</td>
                                <td>{{ $subcontractorwork['contract_amount'] }}</td>
                                <td>{{ $inputHelper::calculate_percentage($subcontractorwork['contract_amount'], $subcontractorwork['total_current_paid_amount']) }}%</td>
                                <td>{{ $numFormat->number_format_by_currency('PHP', $subcontractorwork['total_previous_paid_amount']) }}</td>
                                <td>{{ $numFormat->number_format_by_currency('PHP', $subcontractorwork['total_current_paid_amount']) }}</td>
                                <td>{{ $numFormat->number_format_by_currency('PHP', $subcontractorwork['total_amount_due_left']) }}</td>
                                <td>{{ $subcontractorwork['warranty'] }}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            @if(count($subcontractorworks) > 1)
                {{  $subcontractorworks->setPath(request()->getPathInfo())->appends(\Request::all())->render() }}
            @endif

        </div>
    </div>
@endsection