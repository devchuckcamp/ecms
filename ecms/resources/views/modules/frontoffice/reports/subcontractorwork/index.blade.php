@extends('templates.dmonitoring.master')
@section('title', 'Sub Contractor Monitoring')

@php
    // refactor magic numbers
    $permission = (new \Permission);
    $dateHelper = new \App\Helpers\DateHelper;
    $numFormat = new \App\Helpers\NumberFormatHelper;
    $inputHelper = new \App\Helpers\InputHelper;
    $equipment_list = \App\Helpers\InputHelper::equipment_list();
    $subcontractor_list = \App\Helpers\InputHelper::subcontractor_list();
@endphp

@section('main')
    <div class="main-container-wrapper">
        @include('templates.dmonitoring.includes.page-title', ['page_title' => 'Sub Contractor Work', 'url_back' => 'subcontractor/work'])
        <div class="main-container">

            @include('templates.dmonitoring.includes.subcontractor-submenu')

            <div class="row no-margin margin-bottom-20">
                <div class="col-md-12 filter-wrapper">
                    <div class="col-md-6">
                        {!! Form::open(['method'=>'get', 'class' => 'form-horizontal', 'name' => 'subcontractorwork-filter-form']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::bsInput('text', 'q', request('q'), 'Search', ['placeholder' => 'Transaction ID or Project Name']) }}
                                {{ Form::bsSelect('subcontractor', request('subcontractor'), 'Sub Contractor', $subcontractor_list, ['placeholder' => 'All Sub Contractor']) }}
                            </div>
                            <div class="col-md-6 no-padding">
                                {{ Form::bsDateRangePicker(
                                   ['name' => 'date_from', 'value' => request('date_from'), 'label' => 'From Date', 'attributes' => ['id'=>'date_from'] ],
                                   ['name' => 'date_to', 'value' => request('date_to'), 'label' => 'To Date','attributes' => ['id'=>'date_to'] ]
                               ) }}
                                {{ Form::bsSelect('equipment', request('equipment'), 'Equipment', $equipment_list, ['placeholder' => 'All Equipment']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ url('subcontractor/work') }}" class="btn btn-warning margin-bottom-10">Reset</a>
                                {{ Form::submit('Filter', ['class' => 'btn btn-primary btn-filter margin-bottom-10']) }}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="button" data-url="{{ url('subcontractor/work/create') }}" data-title="Create Work" onclick="CreateModal(this, '#create-work', 'appendViewModal', { modal_size: 'md-custom' }); return false;" class="btn btn-success"><i class="fa fa-table"></i> Create Work</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="table-responsive margin-bottom-20">
                <table class="table table-striped table-bordered table-hover table-ecms">
                    <thead>
                    <tr class="default-header">
                        <th>Transaction ID</th>
                        <th>Date</th>
                        <th>SUBCON</th>
                        <th>Project</th>
                        <th>Equipment (EoR)</th>
                        <th>Scope of Work</th>
                        <th>Contract Amount</th>
                        <th>Accomplishment</th>
                        <th>Previous Paid</th>
                        <th>Current Paid</th>
                        <th>Amount Due Left</th>
                        <th>Warranty Expiry Date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($subcontractorworks) < 1)
                        <tr>
                            <td colspan="26" class="table-no-records">- No records -</td>
                        </tr>
                    @endif
                    @foreach ($subcontractorworks->items() as $subcontractorwork)
                        <tr>
                            <td>{{ $subcontractorwork['transaction_no'] }}</td>
                            <td>{{ $dateHelper->transaction_date($subcontractorwork['transaction_date']) }}</td>
                            <td>{{ $subcontractorwork['subcontractor'] }}</td>
                            <td>{{ $subcontractorwork['project'] }}</td>
                            <td>{{ $subcontractorwork['equipment'] }}</td>
                            <td>{{ $subcontractorwork['scope_of_work'] }}</td>
                            <td>{{ $subcontractorwork['contract_amount'] }}</td>
                            <td>{{ $inputHelper::calculate_percentage($subcontractorwork['contract_amount'], $subcontractorwork['total_current_paid_amount']) }}%</td>
                            <td>{{ $numFormat->number_format_by_currency('PHP', $subcontractorwork['total_previous_paid_amount']) }}</td>
                            <td>{{ $numFormat->number_format_by_currency('PHP', $subcontractorwork['total_current_paid_amount']) }}</td>
                            <td>{{ $numFormat->number_format_by_currency('PHP', $subcontractorwork['total_amount_due_left']) }}</td>
                            <td>{{ $subcontractorwork['warranty'] }}</td>
                            <td>
                                <a href="#" data-url="{{ url('subcontractor/work/edit/'.$subcontractorwork['id']) }}" data-title="{{ $subcontractorwork['project'] }} #{{ $subcontractorwork['transaction_no'] }}" onclick="CreateModal(this, '#edit-subcontractor-work', 'appendViewModal', { modal_size: 'md-custom' }); return false;"><i class="fa fa-pencil"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{  $subcontractorworks->setPath(request()->getPathInfo())->appends(\Request::all())->render() }}

        </div>
    </div>
@endsection