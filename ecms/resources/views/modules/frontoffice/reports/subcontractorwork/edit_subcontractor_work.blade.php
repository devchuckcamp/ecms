@php
    // refactor magic numbers
    $permission = (new \Permission);
    $dateHelper = new \App\Helpers\DateHelper;
    $numFormat = new \App\Helpers\NumberFormatHelper;
    $inputHelper = new \App\Helpers\InputHelper;
    $equipment_list = \App\Helpers\InputHelper::equipment_list();
    $subcontractor_list = \App\Helpers\InputHelper::subcontractor_list();
    $total_accomplishment = $inputHelper::calculate_percentage($subcontractorwork['contract_amount'], $subcontractorwork['total_current_paid_amount']);
@endphp

{{ Form::open(['class'=>'subcontractor-work-edit-form form form-horizontal']) }}
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                {{ Form::bsInput('text', 'transaction_no', $subcontractorwork['transaction_no'], 'Transaction ID', ['readonly']) }}
                {{ Form::bsSelect('subcontractor', $subcontractorwork['subcontractor_id'], 'Sub Contractor', $subcontractor_list) }}

                {{ Form::bsInput('text', 'project', $subcontractorwork['project'], 'Project Name') }}
                {{ Form::bsDatePicker('project_start_date', $dateHelper->human_date($subcontractorwork['project_start_date'], true), 'Project Start Date') }}
                {{ Form::bsInput('text', 'contract_amount', $subcontractorwork['contract_amount'], 'Contract Amount', ['readonly']) }}
            </div>
            <div class="col-md-6 text-area-uniform-height">
                {{ Form::bsDatePicker('transaction_date', $dateHelper->human_date($subcontractorwork['transaction_date'], true), 'Date') }}
                {{ Form::bsSelect('equipment', $subcontractorwork['equipment_id'], 'Equipment', $equipment_list) }}

                {{ Form::bsTextarea('scope_of_work', $subcontractorwork['scope_of_work'], 'Scope of Work') }}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-ecms">
                        <thead>
                        <tr class="default-header">
                            <th>#</th>
                            <th>Date</th>
                            <th>Accomplishment</th>
                            <th>Previous Paid</th>
                            <th>Current Paid</th>
                            <th>Amount Due Left</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(count($subcontractorwork['breakdown_payment']) < 1)
                            <tr>
                                <td colspan="26" class="table-no-records">- No payments made yet -</td>
                            </tr>
                        @endif
                        @foreach ($subcontractorwork['breakdown_payment'] as $k => $payment)
                            <tr>
                                <td>{{ $k + 1 }}</td>
                                <td>{{ $dateHelper->transaction_date($payment['created_at']) }}</td>
                                <td>{{ $inputHelper::calculate_percentage($payment['contract_amount'], $payment['current_paid_amount']) }}%</td>
                                <td>{{ $numFormat->number_format_by_currency('PHP', $payment['previous_paid_amount']) }}</td>
                                <td>{{ $numFormat->number_format_by_currency('PHP', $payment['current_paid_amount']) }}</td>
                                <td>{{ $numFormat->number_format_by_currency('PHP', $payment['amount_due_left']) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                 @if($total_accomplishment != 100)
                    {{ Form::bsInput('text', 'amount_to_pay', old('amount_to_pay'), 'Amount to Pay') }}
                 @else
                    {{ Form::bsInput('text', 'warranty', $subcontractorwork['warranty'], 'Warranty Expiry Date') }}
                @endif
            </div>

            <div class="col-md-6">
                <div class="form-group ">
                    <div class="col-md-12">
                        <label for="warranty" class="control-label input-group-label">
                            Accomplishment
                        </label>
                        <div class="form-input-group">
                            <label for="warranty" class="control-label input-group-label text-success font-size-19">
                                {{ $total_accomplishment }}%
                            </label>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <button type="submit" class="btn btn-success text-uppercase">Submit <i class="fa fa-paper-plane"></i></button>
        </div>
        <div class="pull-right"><span class="tmx"></span></div>
    </div>
</div>
{{ Form::close() }}

<script>
    CodeJquery(function () {

        $('.subcontractor-work-edit-form').submit(function (event) {
            event.preventDefault();

            sendAjax('axios', {
                url: '{{ url('subcontractor/work/edit/'.$subcontractorwork['id']) }}',
                type: 'post',
                data: $(this).serialize(),
                element: $(this)
            });

        });

        createDatePicker('dateInline', { tag: '#transaction_date, #project_start_date', format: 'MMMM DD, YYYY', default_date: '{{ Carbon\Carbon::now()  }}' });

    });
</script>