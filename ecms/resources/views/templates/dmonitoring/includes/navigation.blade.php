@if(env('FRONTOFFICE_DOMAIN') !== $_SERVER['HTTP_HOST'])
    @include('templates.dmonitoring.includes.frontoffice.navigation')
@elseif(env('BACKOFFICE_DOMAIN') == $_SERVER['HTTP_HOST'])
    @include('templates.dmonitoring.includes.backoffice.navigation')
@endif