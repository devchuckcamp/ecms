@if(env('FRONTOFFICE_DOMAIN') !== $_SERVER['HTTP_HOST'])
    @include('templates.dmonitoring.includes.frontoffice.header')
@elseif(env('BACKOFFICE_DOMAIN') == $_SERVER['HTTP_HOST'])
    @include('templates.dmonitoring.includes.backoffice.header')
@endif