<?php

namespace Core\Usecase\SubContractorWork;

use Core\Repository\SubContractorWork\SubContractorWorkRepository;
use Core\Exception;

class SubContractorWorkCreator
{
    public function __construct(SubContractorWorkRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle($subcontractor_work)
    {
        $new_subcontractor_work = $this->repository->store_subcontractor_work([
            'transaction_no' => $subcontractor_work->transaction_no,
            'transaction_date' => $subcontractor_work->transaction_date,
            'created_user_id' => \Auth::user()->id,
            'updated_user_id' => \Auth::user()->id,

            /**
             * For other details
             */
            'subcontractor' => $subcontractor_work->subcontractor,
            'equipment' => $subcontractor_work->equipment,

            'project' => $subcontractor_work->project,
            'scope_of_work' => $subcontractor_work->scope_of_work,
            'project_start_date' => $subcontractor_work->project_start_date,
            'contract_amount' => $subcontractor_work->contract_amount,

        ]);

        return $new_subcontractor_work;
    }
}